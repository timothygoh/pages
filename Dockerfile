FROM jekyll/minimal:latest

COPY . /srv/jekyll

WORKDIR /srv/jekyll

RUN jekyll build

CMD ["jekyll", "serve"]
